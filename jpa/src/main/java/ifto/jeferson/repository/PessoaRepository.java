package ifto.jeferson.repository;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import ifto.jeferson.model.entity.Pessoa;

@Repository
public class PessoaRepository {
    @PersistenceContext
    private EntityManager em;
    public void save(Pessoa pessoa){
        em.persist(pessoa);
    }
    public Pessoa pessoa(Long id){
        return em.find(Pessoa.class, id);
    }
    public List<Pessoa> pessoas(){
        Query query = em.createQuery("from Pessoa");
        return query.getResultList();
    }
    public void remove(Long id){
        Pessoa p = em.find(Pessoa.class, id);
        em.remove(p);
    }
    public void update(Pessoa pessoa){
        em.merge(pessoa);
    }
}